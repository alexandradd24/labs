#include <iostream>
#include <iomanip>
#include <vector>

template<typename T>
class Matrix {
public:
	Matrix();                                      // Конструктор пустой матрицы
	Matrix(size_t rows, size_t cols);              // Конструктор матрицы заданных размеров
	Matrix(const std::vector<std::vector<T>>& v);  // Конструктор матрицы из вектора векторов
	Matrix(const Matrix<T>& other);                // Конструктор копирования

	Matrix<T>& operator=(const Matrix<T>& other);  // Оператор присваивания

	T& operator()(size_t i, size_t j);              // Доступ к элементу (i, j)
	const T& operator()(size_t i, size_t j) const;  // Доступ к элементу (i, j) для константной матрицы

	size_t get_rows() const;  // Получение количества строк
	size_t get_cols() const;  // Получение количества столбцов

	bool operator==(const Matrix<T>& other) const;  // Сравнение матриц
	bool operator!=(const Matrix<T>& other) const;  // Сравнение матриц

	Matrix<T> operator*(const T& x) const;              // Умножение матрицы на элемент
	Matrix<T> operator+(const Matrix<T>& other) const;  // Сложение матриц
	Matrix<T> operator-(const Matrix<T>& other) const;  // Вычитание матриц
	Matrix<T> operator-() const;                        // Отрицание матрицы
	Matrix<T> operator*(const Matrix<T>& other) const;  // Умножение матриц

	Matrix<T> transpose() const;  // Транспонирование матрицы

	// Получение подматрицы, начиная с позиции (row, col) и размерами (rows, cols)
	Matrix<T> submatrix(size_t row, size_t col, size_t rows, size_t cols) const;

	static Matrix<T> eye(size_t n);                    // Создание единичной матрицы
	static Matrix<T> zeros(size_t rows, size_t cols);  // Создание нулевой матрицы

	Matrix<T> horizontal_concatenate(const Matrix<T>& other) const;  // Конкатенация матриц по горизонтали
	Matrix<T> vertical_concatenate(const Matrix<T>& other) const;    // Конкатенация матриц по вертикали

	// «Красивый» вывод матрицы
	template <typename Tstream>
	friend std::ostream &operator<<(std::ostream &out, const Matrix<Tstream>& m);

private:
	size_t rows;                      // количество строк
	size_t cols;                      // количество столбцов
	std::vector<std::vector<T>> data; // данные матрицы
};

template <typename Tstream>
std::ostream &operator<<(std::ostream &out, const Matrix<Tstream>& m) {
	const int MAX_NUM_DIGITS = 5;
	out << std::endl;
	for (int i = 0; i < m.rows; ++i) {
		for (int j = 0; j < m.cols; ++j) {
			out << std::setw(MAX_NUM_DIGITS) << m(i, j) << " ";
		}
		out << std::endl;
	}
	return out;
}

template<typename T>
Matrix<T>::Matrix()
{
	this->rows = 0;
	this->cols = 0;
	this->data = std::vector<std::vector<T>>();
}

template<typename T>
Matrix<T>::Matrix(size_t rows, size_t cols)
{
	this->rows = rows;
	this->cols = cols;
	this->data = std::vector<std::vector<T>>(rows, std::vector<T>(cols));
}

template<typename T>
Matrix<T>::Matrix(const std::vector<std::vector<T>> &v)
{
	if (v.empty())
	{
		this->rows = 0;
		this->cols = 0;
	}
	else
	{
		size_t colonki = v[0].size();
		for (int i = 0; i < v.size(); ++i)
		{
			if(colonki!=v[i].size())
				throw std::invalid_argument("В векторах разное количество элементов. Не удаётся создать матрицу");
		}
		this->rows = v.size();
		this->cols = v[0].size();
	}
	this->data = v;
}


template<typename T>
Matrix<T>::Matrix(const Matrix<T>& other)
{
	this->rows = other.rows;
	this->cols = other.cols;
	this->data = other.data;
}

template<typename T>
Matrix<T> &Matrix<T>::operator=(const Matrix<T> &other)
{
	if (&other != this)
	{
		this->rows = other.rows;
		this->cols = other.cols;
		this->data = other.data;
	}
	return *this;
}

template<typename T>
T &Matrix<T>::operator()(size_t i, size_t j)
{
	return this->data[i][j];
}

template<typename T>
const T &Matrix<T>::operator()(size_t i, size_t j) const
{
	return this->data[i][j];
}

template<typename T>
size_t Matrix<T>::get_rows() const
{
	return this->rows;
}

template<typename T>
size_t Matrix<T>::get_cols() const
{
	return this->cols;
}

template<typename T>
bool Matrix<T>::operator==(const Matrix<T> &other) const
{
	if (this->rows != other.rows || this->cols != other.cols)
		return false;
	for (int i = 0; i < other.rows; ++i)
	{
		for (int j = 0; j < other.cols; ++j)
		{
			if (this->data[i][j] != other.data[i][j])
				return false;
		}
	}
	return true;
}

template<typename T>
bool Matrix<T>::operator!=(const Matrix<T> &other) const
{
	if (this->rows != other.rows || this->cols != other.cols)
		return true;
	for (int i = 0; i < other.rows; ++i)
	{
		for (int j = 0; j < other.cols; ++j)
		{
			if (this->data[i][j] != other.data[i][j])
				return true;
		}
	}
	return false;
}

template<typename T>
Matrix<T> Matrix<T>::operator*(const T& x) const
{
	Matrix<T> mat(this->rows, this->cols);
	for (int i = 0; i < this->rows; ++i)
	{
		for (int j = 0; j < this->cols; ++j)
		{
			mat.data[i][j] = this->data[i][j] * x;
		}
	}
	return mat;
}

template<typename T>
Matrix<T> Matrix<T>::operator+(const Matrix<T> &other) const
{
	if (this->rows != other.rows || this->cols != other.cols)
		throw std::invalid_argument("Сложить матрицы не получиться. У них разное количество строк или столбцов");
	Matrix<T> mat(this->rows, this->cols);
	for (int i = 0; i < this->rows; ++i)
	{
		for (int j = 0; j < this->cols; ++j)
		{
			mat.data[i][j] = this->data[i][j] + other.data[i][j];
		}
	}
	return mat;
}

template<typename T>
Matrix<T> Matrix<T>::operator-(const Matrix<T>& other) const
{
	if (this->rows != other.rows || this->cols != other.cols)
		throw std::invalid_argument("Вычесть матрицы не получиться. У них разное количество строк или столбцов");
	Matrix<T> mat(this->rows, this->cols);
	for (int i = 0; i < this->rows; ++i)
	{
		for (int j = 0; j < this->cols; ++j)
		{
			mat.data[i][j] = this->data[i][j] - other.data[i][j];
		}
	}
	return mat;
}

template<typename T>
Matrix<T> Matrix<T>::operator-() const
{
	Matrix<T> mat(this->rows, this->cols);
	for (int i = 0; i < this->rows; ++i)
	{
		for (int j = 0; j < this->cols; ++j)
		{
			mat.data[i][j] = this->data[i][j] * (-1);
		}
	}
	return mat;
}

template<typename T>
Matrix<T> Matrix<T>::operator*(const Matrix<T>& other) const
{
	if (this->cols != other.rows)
		throw std::invalid_argument("Умножить матрицы не получиться. Количество строк первой матрицы не совпадает с количеством столбцов второй");
	Matrix<T> mat(this->rows, other.cols);
	int a = 0;
	for (int i = 0; i < this->rows; ++i)
	{
		for (int j = 0; j < other.cols; ++j)
		{
			a = 0;
			for (int l = 0; l < this->cols; ++l)
				a += this->data[i][l] * other.data[l][j];
			mat.data[i][j] = a;
		}
	}
	return mat;
}

template<typename T>
Matrix<T> Matrix<T>::transpose() const
{
	Matrix<T> mat(this->cols, this->rows);
	for (int i = 0; i < mat.rows; ++i)
	{
		for (int j = 0; j < mat.cols; ++j)
		{
			mat.data[i][j] = this->data[j][i];
		}
	}
	return mat;
}

template<typename T>
Matrix<T> Matrix<T> ::submatrix(size_t row, size_t col, size_t rows, size_t cols) const
{
	if (row + rows > this->rows || col + cols > this->cols || row < 0 || col < 0)
		throw std::invalid_argument("Неправильно задана начальная позиция или размеры подматрицы выходят за границы начальной матрицы");
	Matrix<T> mat(rows, cols);
	for (int i = row; i < row + mat.rows; ++i)
	{
		for (int j = col; j < col + mat.cols; ++j)
		{
			mat.data[i-row][j-col] = this->data[i][j];
		}
	}
	return mat;
}


template<typename T>
Matrix<T> static Matrix<T>::eye(size_t n)
{
	Matrix<T> mat(n, n);
	for (int i = 0; i < n; ++i)
	{
		for (int j = 0; j < n; ++j)
		{
			if (i == j)
				mat.data[i][j] = 1;
			else
				mat.data[i][j] = 0;
		}
	}
	return mat;
}

template<typename T>
Matrix<T> static Matrix<T>::zeros(size_t rows, size_t cols)
{
	Matrix<T> mat(rows, cols);
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; ++j)
		{
			mat.data[i][j] = 0;
		}
	}
	return mat;
}

template<typename T>
Matrix<T> Matrix<T> ::horizontal_concatenate(const Matrix<T>& other) const
{
	if (this->rows != other.rows)
		throw std::invalid_argument("Количество строк у матриц не совпадает");
	Matrix<T> mat(this->rows, other.cols+this->cols);
	for (int i = 0; i < this->rows; ++i)
	{
		for (int j = 0; j < this->cols; ++j)
		{
			mat.data[i][j] = this->data[i][j];
		}
	}
	for (int i = 0; i < other.rows; ++i)
	{
		for (int j = 0; j < other.cols; ++j)
		{
			mat.data[i][j + this->cols] = other.data[i][j];
		}
	}
	return mat;
}

template<typename T>
Matrix<T> Matrix<T>::vertical_concatenate(const Matrix<T>& other) const
{
	if (this->cols != other.cols)
		throw std::invalid_argument("Количество столбцов у матриц не совпадает");
	Matrix<T> mat(this->rows+other.rows, this->cols);
	for (int i = 0; i < this->rows; ++i)
	{
		for (int j = 0; j < this->cols; ++j)
		{
			mat.data[i][j] = this->data[i][j];
		}
	}
	for (int i = 0; i < other.rows; ++i)
	{
		for (int j = 0; j < other.cols; ++j)
		{
			mat.data[i + this->rows][j] = other.data[i][j];
		}
	}
	return mat;
}


