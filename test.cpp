﻿#include <iostream>
#include <iomanip>
#include <vector>
#include "matrix.h"

int main() 
{
	std::vector<std::vector<int>> v1 = { {1,2}, {3, 4} };
	std::vector<std::vector<int>> v2 = { {5,6}, {7, 8} };
	Matrix<int> m1(v1);
	Matrix<int> m2(v2);
	Matrix<int> m3();
	Matrix<int> m4(3,3);
	Matrix<int> m(m1);
	bool f = m1 == m2;
	f = m1 != m4;

	m = m2;
	m = m1 + m2;
	m = m1 - m2;
	m = m1 * m2;
	m = -m1;
	m = m1 * 2;
	m = m1.transpose();
	m = m1.submatrix(1, 0, 1, 2);
	m = m1.horizontal_concatenate(m2);
	m = m1.vertical_concatenate(m2);
	std::cout << m;
	std::cout << m1(0,0)<<std::endl;
	std::cout << m1.get_cols() << std::endl;
	std::cout << m4.get_rows() << std::endl;
	std::cout <<f << std::endl;
	return 0;
}